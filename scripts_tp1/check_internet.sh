#!/bin/bash

# test du ping vers google
echo "[...] Checking internet connection [...]"
nc -z 8.8.8.8 53 >/dev/null
# $? indique le code de retour de la dernière commande
test=$?
# boucle qui vérifie si connexion ok, alors on affiche la valeur 0
if [ $test -eq 0 ]; then
	echo "[...] Internet access ok [...]"
	exit 0
# si connexion non ok, on affiche la valeur 1
else 
	echo "[/!\] Not connected to Internet [/!\]"
	echo "[/!\] Please check configuration [/!\]"
	exit 1
fi
