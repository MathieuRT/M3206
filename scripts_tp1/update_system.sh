#!/bin/bash

# on lance la mise à jour du système, on redirige le résulat car on ne souhaite pas le sauvegardé
apt-get update >/dev/null

# Teste si on a les droits pour lancer la commande de mise à jour 
if [ $UID -eq 0 ]; then
	echo "[...] update database 	[...]"
	echo "[...] upgrade database 	[...]"
	exit 0
# si ce n'est pas le cas, on informe que le mode de connexion doit être changé
else
	echo "[/!\] Vous devez être super-utilisateur [/!\]"
	exit 1
fi
