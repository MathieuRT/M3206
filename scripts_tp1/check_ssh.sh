#!/bin/bash

# Teste si le daemon ssh est intallé et lancé
apt-get install openssh-server>/dev/null
/etc/init.d/ssh status>/dev/null

daemon=$?
if [ $daemon -eq 0 ]; then
	echo "[...] ssh: est fonctionne 	   [...]"
	exit 0
else
	echo "[...] ssh: est installé		   [...]"
	echo "[/!\] ssh: le service n'est pas lancé[/!\]"
	echo "[/!\] ssh: lancement du service  	   [/!\] lancez la commande /ec/init.d/ssh start"
	exit 1
fi
