#!/bin/bash

apt-get install git tmux htop>/dev/null
paquet=$?
if [ $paquet -eq 0 ]; then
	echo "[...] git: installé 	[...]"
	echo "[...] tmux: installé 	[...]"
	echo "[/!\] vim: pas installé 	[/!\] lancer la commande apt-get install vim"
	echo "[...] htop: installé	[...]"
	exit 0
fi
